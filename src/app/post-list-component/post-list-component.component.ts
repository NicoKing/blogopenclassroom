import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post.model';
import { Subscription, of } from 'rxjs';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';
import { post } from 'selenium-webdriver/http';

@Component({
  selector: 'app-post-list-component',
  templateUrl: './post-list-component.component.html',
  styleUrls: ['./post-list-component.component.css']
})
export class PostListComponentComponent implements OnInit {

  posts: Post[];
  postSubscription: Subscription;

  // posts = [
  //   {
  //     titre: 'Mon premier post',
  //     contenu: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
  //     date: new Date()
  //   },
  //   {
  //     titre: 'Mon deuxième post',
  //     contenu: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
  //     date: new Date()
  //   },
  //   {
  //     titre: 'Encore un post',
  //     contenu: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
  //     date: new Date()
  //   }
  // ];

  constructor(private postService: PostsService,
              private router: Router) { }

  ngOnInit() {
    this.postSubscription = 
      this.postService.postsSubject.subscribe(
        (posts: Post[]) => {
          this.posts = posts;
        }
      );
      this.postService.getPosts();
  }

  // onNewPost(){
  //   this.router.navigate(['/posts', 'new']);
  // }

  ngOnDestroy(){
    this.postSubscription.unsubscribe();
  }

}
