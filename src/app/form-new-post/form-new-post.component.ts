import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Post } from '../models/post.model';
import { PostsService } from '../services/posts.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form-new-post',
  templateUrl: './form-new-post.component.html',
  styleUrls: ['./form-new-post.component.css']
})
export class FormNewPostComponent implements OnInit {

  postForm: FormGroup;
  
  constructor(private formBuilder: FormBuilder,
              private postService: PostsService,
              private router: Router) { }

  ngOnInit() {
    this.initForm();
  }

  initForm(){
    this.postForm = this.formBuilder.group({
      titre: ['', Validators.required],
      contenu: ['', Validators.required]
    });
  }

  onSavePost(){
    const titre = this.postForm.get('titre').value;
    const contenu = this.postForm.get('contenu').value;
    const newPost = new Post(titre, contenu);
    this.postService.createPost(newPost);
    this.router.navigate(['/posts']);

  }
}
