import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(){
    const config = {
      apiKey: "AIzaSyC5jI72ttsLwfxOafOaxOQ0yH_e1q88sPE",
    authDomain: "blogopenclassroom.firebaseapp.com",
    databaseURL: "https://blogopenclassroom.firebaseio.com",
    projectId: "blogopenclassroom",
    storageBucket: "",
    messagingSenderId: "6781372536"
  };
  firebase.initializeApp(config);
  }
}
