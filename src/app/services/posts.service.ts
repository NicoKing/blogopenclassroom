import { Injectable } from '@angular/core';
import { Subject, of } from 'rxjs';
import { Post } from '../models/post.model';
import * as firebase from 'firebase';
import DataSnapshot = firebase.database.DataSnapshot;

@Injectable()
export class PostsService {

    posts: Post[] = [];
    postsSubject = new Subject<Post[]>();

    emitPosts(){
        this.postsSubject.next(this.posts);
    }

    savePosts(){
        firebase.database().ref('/posts').set(this.posts);
    }

    getPosts(){
        firebase.database().ref('/posts').on('value',(data: DataSnapshot) => {
            this.posts = data.val();
            this.posts = this.posts ? this.posts : [];
            this.emitPosts();
            }
        );
    }

    createPost(newPost: Post){
        this.posts.push(newPost);
        console.log('juste avant la sauvegarde :' + this.posts[this.posts.length - 1].created_at );
        this.savePosts();
        this.emitPosts();
    }

    removePost(post : Post){
        const postIndexToRemove = this.posts.findIndex(
            (postE1) => {
                if (postE1 === post){
                    return true;
                }
            }
        );
        this.posts.splice(postIndexToRemove, 1);
        this.savePosts();
        this.emitPosts();
    }

}