import { DatePipe } from '@angular/common';

export class Post {
    loveIts: number;
    created_at: string;
    private dateToConvert : Date;
    private datePipe : DatePipe;

    constructor(public titre: string, 
                public contenu: string
                ){
                  this.loveIts = 0;
                  this.dateToConvert = new Date();
                  this.created_at = this.dateToConvert.toDateString();
                }

  }