import { Component, Input, OnInit } from '@angular/core';
import { post } from 'selenium-webdriver/http';
import { Post } from '../models/post.model';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-post-list-item-component',
  templateUrl: './post-list-item-component.component.html',
  styleUrls: ['./post-list-item-component.component.css']
})
export class PostListItemComponentComponent implements OnInit {

  @Input() title:       string;
  @Input() content:     string; 
  @Input() loveIts:     number;
  @Input() created_at:  Date;
  @Input() post:        Post;

  constructor(private postsService: PostsService) { 

    this.loveIts = 0;

  }

  ngOnInit() {
    
  }

  loveItsFunction(){
    this.post.loveIts = this.post.loveIts + 1;
    this.postsService.savePosts();
  }

  dontLoveItsFunction(){
    this.post.loveIts = this.post.loveIts - 1;
    this.postsService.savePosts();
  }

  onDeletePost(post : Post){
    this.postsService.removePost(post);
  }

}
